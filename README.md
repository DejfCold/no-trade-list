# The No-trade list

This contains a list of companies, that is not advised to (according to my own experience) form any partnership with.

Unless you want to do questionable stuff. Then they might be ideal for you.

(sorted in no particular order)

## Reality

- MOVITO group, s. r. o. (and companies associated with it's owner)
- Flake s. r. o. (aka Century 21 VIP under the Century 21 franchise) (and companies associated with it's owner)

## Trading

- traderspros.com (see [trustpilot](https://www.trustpilot.com/review/traderspros.com) and this [tweet](https://twitter.com/mer_grl/status/1394619533663551491) or it's [tweetstamp](https://tweetstamp.org/1394619533663551491))
